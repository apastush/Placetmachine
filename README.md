# Placetmachine
The package features the Placet interfaced with Python and the class extending its functionality

# Package installation

1. Clone the repository to your local machine

2. When in the folder run:
```
    pip3 install .
```
The package should be installed and available as `placetmachine`.

**It requires a PLACET installation (https://gitlab.cern.ch/clic-software/placet)!**

***This is a mirror repository. Main one is stored at https://github.com/drozzoff/Placetmachine***
